package main

import (
	"database/sql"
	"fmt"
	"os"
	"simple-rest-api/server"

	_ "github.com/lib/pq"
)

func main() {

	db, err := newDB("postgres")
	if err != nil {
		panic(err)
	}

	server.Server(db)
}

func newDB(driver string) (*sql.DB, error) {
	dsn := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		os.Getenv("DATABASE_HOST"),
		os.Getenv("DATABASE_PORT"),
		os.Getenv("DATABASE_USER"),
		os.Getenv("DATABASE_PASSWORD"),
		os.Getenv("DATABASE_DBNAME"),
	)

	db, err := sql.Open(driver, dsn)
	if err != nil {
		return nil, err
	}

	db.SetMaxOpenConns(50)
	db.SetMaxIdleConns(25)
	return db, nil
}

package domain

type JSONWebToken struct {
	AccessToken  string
	RefreshToken string
	AccessUuid   string
	RefreshUuid  string
	AtExpires    int64
	RtExpires    int64

	UserID      uint64
	Email       string
	Permissions Permissions
}

package domain

import (
	"fmt"
	"simple-rest-api/domain/types"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"github.com/pkg/errors"
)

type ContextKey string

var ContextKeyUser = ContextKey("user")

type User struct {
	ID          uint64
	Name        string
	Email       string
	Password    types.Password
	Permissions Permissions
}

var AnonymousUser = &User{}

func (u *User) IsAnonymous() bool {
	return u == AnonymousUser
}

func (u *User) GenerateJWT(secretAT, secretRT string) (tkn *JSONWebToken, err error) {
	accessUUID := uuid.New()

	tkn = &JSONWebToken{
		AccessUuid:  accessUUID.String(),
		RefreshUuid: fmt.Sprintf("%s--%d", accessUUID, u.ID),
		AtExpires:   time.Now().Add(time.Minute * 30).Unix(),
		RtExpires:   time.Now().Add(time.Hour * 24 * 7).Unix(),
		UserID:      u.ID,
		Email:       u.Email,
	}

	atClaims := jwt.MapClaims{}
	atClaims["access_uuid"] = tkn.AccessUuid
	atClaims["authorized"] = true
	atClaims["user_id"] = u.ID
	atClaims["email"] = u.Email
	atClaims["permissions"] = u.Permissions
	atClaims["exp"] = tkn.AtExpires

	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	tkn.AccessToken, err = at.SignedString([]byte(secretAT))
	if err != nil {
		return nil, errors.Wrap(err, "internal server error - contact support")
	}

	rtClaims := jwt.MapClaims{}
	rtClaims["refresh_uuid"] = tkn.RefreshUuid
	rtClaims["user_id"] = u.ID
	rtClaims["email"] = u.Email
	rtClaims["permissions"] = u.Permissions
	rtClaims["exp"] = tkn.RtExpires
	rt := jwt.NewWithClaims(jwt.SigningMethodHS256, rtClaims)
	tkn.RefreshToken, err = rt.SignedString(([]byte(secretRT)))
	if err != nil {
		return nil, errors.Wrap(err, "internal server error - contact support")
	}

	return tkn, nil
}

type Permissions []string

func (p Permissions) Include(code string) bool {
	for i := range p {
		if code == p[i] {
			return true
		}
	}

	return false
}

package service

import (
	"context"
	"errors"
	"os"
	"simple-rest-api/decoder"
	"simple-rest-api/domain"
)

var (
	secretAT = os.Getenv("SECRET_ACCESS_TOKEN")
	secretRT = os.Getenv("SECRET_REFRESH_TOKEN")
)

type UserRepository interface {
	InsertOrIgnore(ctx context.Context, user *domain.User) error
	GetByEmail(ctx context.Context, email string) (*domain.User, error)
	GetAll(ctx context.Context) ([]*domain.User, error)
	GetByID(ctx context.Context, id uint64) (*domain.User, error)
}

type UserService struct {
	repo UserRepository
}

func NewUserService(repo UserRepository) *UserService {
	return &UserService{repo}
}

func (u *UserService) Register(ctx context.Context, user *domain.User) error {

	user.Permissions = append(user.Permissions, "user:read", "user:write")
	return u.repo.InsertOrIgnore(ctx, user)
}

func (u *UserService) Login(ctx context.Context, email, password string) (*domain.JSONWebToken, error) {
	user, err := u.repo.GetByEmail(ctx, email)
	if err != nil {
		return nil, errors.New("user not found")
	}

	match, err := user.Password.Matches(password)
	if err != nil {
		return nil, errors.New("internal server error - contact support")
	}

	if !match {
		return nil, errors.New("bad email and password combination")
	}

	t, err := user.GenerateJWT(secretAT, secretRT)
	if err != nil {
		return nil, err
	}

	return t, nil
}

func (u *UserService) Refresh(ctx context.Context, token string) (*domain.JSONWebToken, error) {
	jwtToken, err := decoder.VerifyJWT(token, secretRT)
	if err != nil {
		return nil, err
	}

	user := &domain.User{
		ID:          jwtToken.UserID,
		Email:       jwtToken.Email,
		Permissions: jwtToken.Permissions,
	}

	t, err := user.GenerateJWT(secretAT, secretRT)
	if err != nil {
		return nil, err
	}

	return t, nil
}

func (u *UserService) GetAll(ctx context.Context) ([]*domain.User, error) {
	return u.repo.GetAll(ctx)
}

func (u *UserService) GetByID(ctx context.Context, id uint64) (*domain.User, error) {
	return u.repo.GetByID(ctx, id)
}

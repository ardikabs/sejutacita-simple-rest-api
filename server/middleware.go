package server

import (
	"context"
	"errors"
	"net/http"
	"os"
	"simple-rest-api/decoder"
	"simple-rest-api/domain"
	"simple-rest-api/handler"
	"strings"

	"github.com/labstack/echo/v4"
)

func JWTDecoderMiddleware(decoder decoder.JWTDecoder) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			ctx := c.Request().Context()
			authHeader := c.Request().Header.Get(echo.HeaderAuthorization)
			if authHeader == "" {
				reqCtx := context.WithValue(ctx, domain.ContextKeyUser, domain.AnonymousUser)
				req := c.Request().Clone(reqCtx)
				c.SetRequest(req)
				return next(c)
			}

			var authBerareKey = "Bearer"
			token := strings.Split(authHeader, " ")
			if len(token) != 2 || len(token) == 2 && (token[0] != authBerareKey) {
				return c.JSON(http.StatusUnauthorized, handler.NewError(errors.New("unauthorized access")))
			}

			secret := os.Getenv("SECRET_ACCESS_TOKEN")
			user, err := decoder.Decode(token[1], secret)
			if err != nil {
				return c.JSON(http.StatusUnauthorized, handler.NewError(err))
			}

			reqCtx := context.WithValue(ctx, domain.ContextKeyUser, user)
			req := c.Request().Clone(reqCtx)
			c.SetRequest(req)
			return next(c)
		}
	}
}

func PermissionMiddleware(code string) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			ctx := c.Request().Context()
			user, err := handler.ExtractUserFromContext(ctx)
			if err != nil {
				return c.JSON(http.StatusUnauthorized, handler.NewError(errors.New("unauthorized access")))
			}

			if !user.Permissions.Include(code) {
				return c.JSON(http.StatusForbidden, handler.NewError(errors.New("forbidden access")))
			}

			return next(c)
		}
	}
}

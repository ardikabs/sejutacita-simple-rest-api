package server

import (
	"database/sql"
	"simple-rest-api/decoder"
	"simple-rest-api/handler"
	"simple-rest-api/repository"
	"simple-rest-api/service"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func Server(db *sql.DB) {
	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
	}))

	jwtDecoder := decoder.NewJWTDecoder()
	userRepo := repository.NewUserRepository(db)
	userSvc := service.NewUserService(userRepo)

	e.Use(JWTDecoderMiddleware(jwtDecoder))
	e.POST("/api/v1/auth/register", handler.AuthRegister(userSvc))
	e.POST("/api/v1/auth/login", handler.AuthLogin(userSvc))
	e.POST("/api/v1/auth/refresh", handler.AuthRefresh(userSvc))
	e.GET("/api/v1/users", handler.UserFetchAll(userSvc), PermissionMiddleware("user:admin"))
	e.GET("/api/v1/users/:id", handler.UserFetchByID(userSvc), PermissionMiddleware("user:read"))
	e.GET("/healthz", handler.Healthz(db))

	e.Logger.Fatal(e.Start(":8080"))
}

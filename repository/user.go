package repository

import (
	"context"
	"database/sql"
	"errors"
	"simple-rest-api/domain"
	"strings"

	"github.com/lib/pq"
)

type UserRepository struct {
	db *sql.DB
}

func NewUserRepository(db *sql.DB) *UserRepository {
	return &UserRepository{
		db: db,
	}
}

func (u *UserRepository) InsertOrIgnore(ctx context.Context, user *domain.User) error {
	tx, err := u.db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if user.Name == "" {
		user.Name = strings.Split(user.Email, "@")[0]
	}

	if err := tx.QueryRowContext(ctx, `
		INSERT INTO users(
			name,
			email,
			password
		)
		VALUES ($1, $2, $3)
		ON CONFLICT(email)
		DO NOTHING
		RETURNING id;
	`, user.Name, user.Email, user.Password).Scan(
		&user.ID,
	); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			// mean user already exist
			return nil
		}
		return err
	}

	if err := u.addPermissionsForUser(tx, user.ID, user.Permissions...); err != nil {
		return err
	}

	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

func (u *UserRepository) GetByEmail(ctx context.Context, email string) (*domain.User, error) {
	out := &domain.User{}

	if err := u.db.QueryRowContext(ctx, `
		SELECT id, email, password
		FROM users
		WHERE email = $1
	`,
		email,
	).Scan(
		&out.ID,
		&out.Email,
		&out.Password,
	); err != nil {
		return nil, err
	}

	permissions, err := u.getAllPermissionsByUser(ctx, out.ID)
	if err != nil {
		return nil, err
	}

	out.Permissions = permissions

	return out, nil
}

func (u *UserRepository) GetAll(ctx context.Context) ([]*domain.User, error) {
	out := make([]*domain.User, 0)

	rows, err := u.db.QueryContext(ctx, `
		SELECT id, name, email
		FROM users
	`)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		user := &domain.User{}

		if err := rows.Scan(
			&user.ID,
			&user.Name,
			&user.Email,
		); err != nil {
			return nil, err
		}

		out = append(out, user)
	}

	return out, nil
}

func (u *UserRepository) GetByID(ctx context.Context, id uint64) (*domain.User, error) {
	out := &domain.User{}

	if err := u.db.QueryRowContext(ctx, `
		SELECT id, name, email
		FROM users
		WHERE id = $1
	`, id).Scan(
		&out.ID,
		&out.Name,
		&out.Email,
	); err != nil {
		return nil, err
	}

	return out, nil
}

func (u *UserRepository) getAllPermissionsByUser(ctx context.Context, userID uint64) (domain.Permissions, error) {
	query := `
        SELECT permissions.code
        FROM permissions
        INNER JOIN users_permissions ON users_permissions.permission_id = permissions.id
        INNER JOIN users ON users_permissions.user_id = users.id
        WHERE users.id = $1`

	rows, err := u.db.QueryContext(ctx, query, userID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var permissions domain.Permissions

	for rows.Next() {
		var permission string

		err := rows.Scan(&permission)
		if err != nil {
			return nil, err
		}

		permissions = append(permissions, permission)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}

	return permissions, nil
}

func (u *UserRepository) addPermissionsForUser(tx *sql.Tx, userID uint64, codes ...string) error {
	query := `
	INSERT INTO users_permissions
	SELECT $1, permissions.id FROM permissions WHERE permissions.code = ANY($2)`

	_, err := tx.Exec(query, userID, pq.Array(codes))
	return err
}

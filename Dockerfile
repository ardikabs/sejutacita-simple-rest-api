FROM golang:1.16-bullseye AS builder
WORKDIR /app
COPY . .
RUN make build


FROM debian:bullseye-slim
WORKDIR /app
COPY --from=builder /app/simple-rest-api .

EXPOSE 8080
CMD ["/app/simple-rest-api"]
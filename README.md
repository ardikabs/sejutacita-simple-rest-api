# simple-rest-api

## Description

This is an example application build with Go language, with some features consist of:
1. User management, like login, register, refresh token
2. Authentication built on top of JSON Web Token

The application code itself more or less implement Clean Code pattern.

## Diagram Flow
```bash
<end-user> - <simple-rest-api> - <database postgreSQL>
```

Basically it just simple client - server architecture with designed authorization.
Some of the endpoints is protected and only access token with predefined permission is permitted, such as:
1. `user:admin`, is permission to access `/api/v1/users` (Fetch All Users)
2. `user:read`, is permission to access `/api/v1/users/:id` (Fetch User By ID), but user with point 1 permission is able fetch all available IDs.
    Otherwise, the user only able to fetch their id.

## Development Guide
### Prerequisites
* Makefile
* direnv - to be able to trigger `.envrc`
* Go 1.16
* PostgreSQL 13.4

### Setup
* Install Git
* Install Go 1.16
* Install Make to be able run with Makefile
* Replace any credentials on file `.envrc` following with your needs
* Run `direnv allow`

### Build and run binary file
This application is equipped with Makefile, so I assume you know about Makefile.<br>
To be able run this application on local workstation you need to run:
* `make run`

To build the application locally you need to run:
* `make build`

To breakdown support command on `make`, you need to run:
* `make help`

### Notes
If you are willing to seeds the admin user credentials you need to run the following commands:
* `go run db/seed/main.go`
It will produce the admin credentials with the following details:
```bash
user: admin@sejutacita.id
pass: 1+1samadengan2
```

## Deployment
The deployment stack, we utilize GitlabCI to construct the CI/CD process that consist of the following stages:
1. Lint - to lint the code so we got consistent pattern
2. Build - to build the application code and forming container image
3. Deploy - to deploy the application into Kubernetes cluster, all the manifest is stored on [deploy](./deploy) directories, we used [Kustomize](https://kustomize.io/) to manage Kubernetes manifests.

Consider to check the CI/CD pages.
## SAYONARA
After all, you can check it out with the followings:
* Endpoint [here](https://sejutacita.ardikabs.com)
* Postman [here](./postman)
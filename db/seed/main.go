package main

import (
	"context"
	"database/sql"
	"fmt"
	"os"
	"simple-rest-api/domain"
	"simple-rest-api/repository"

	_ "github.com/lib/pq"
)

func main() {

	db, err := newDB("postgres")
	if err != nil {
		panic(err)
	}

	user := &domain.User{
		Email: "admin@sejutacita.id",
	}

	if err := user.Password.Set("1+1samadengan2"); err != nil {
		panic(err)
	}

	repo := repository.NewUserRepository(db)
	user.Permissions = append(user.Permissions,
		"user:admin",
		"user:read",
		"user:write",
	)
	if err := repo.InsertOrIgnore(context.TODO(), user); err != nil {
		panic(err)
	}

	fmt.Println("db.seed: admin user has been added")
}

func newDB(driver string) (*sql.DB, error) {
	dsn := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		os.Getenv("DATABASE_HOST"),
		os.Getenv("DATABASE_PORT"),
		os.Getenv("DATABASE_USER"),
		os.Getenv("DATABASE_PASSWORD"),
		os.Getenv("DATABASE_DBNAME"),
	)

	db, err := sql.Open(driver, dsn)
	if err != nil {
		return nil, err
	}

	db.SetMaxOpenConns(50)
	db.SetMaxIdleConns(25)
	return db, nil
}

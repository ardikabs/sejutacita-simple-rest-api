BEGIN;

CREATE TABLE IF NOT EXISTS users (
   id              BIGSERIAL      PRIMARY KEY,
   "name"          TEXT           NOT NULL,
   email           TEXT           UNIQUE NOT NULL,
   "password"      TEXT           NOT NULL
);

COMMIT;
OUTDIR := bin
GOMIGRATE_VERSION = 4.14.1
GOLANGCI_VERSION = 1.31.0
PLATFORM := $(shell uname | tr '[:upper:]' '[:lower:]')
PATH := bin:$(PATH)
SHELL := env PATH=$(PATH) /bin/bash

## help: print this help message
.PHONY: help
help:
	@echo 'Usage:'
	@sed -n 's/^##//p' ${MAKEFILE_LIST} | column -t -s ':' |  sed -e 's/^/ /'

.PHONY: confirm
confirm:
	@echo -n 'Are you sure? [y/N] ' && read ans && [ $${ans:-N} = y ]

bin/golangci-lint: bin/golangci-lint-${GOLANGCI_VERSION}
	@ln -sf golangci-lint-${GOLANGCI_VERSION} bin/golangci-lint

bin/golangci-lint-${GOLANGCI_VERSION}:
	@mkdir -p $(OUTDIR)
	curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | bash -s -- -b ./bin/ v${GOLANGCI_VERSION}
	@mv bin/golangci-lint "$@"

MIGRATE := $(shell command -v migrate 2>/dev/null)
bin/migrate:
ifndef MIGRATE
	@mkdir -p $(OUTDIR)
	curl -L https://github.com/golang-migrate/migrate/releases/download/v${GOMIGRATE_VERSION}/migrate.${PLATFORM}-amd64.tar.gz | tar -xzO ./migrate.${PLATFORM}-amd64 > bin/migrate
	@chmod +x bin/migrate
endif

## audit: tidy and vendor dependencies and format, vet, lint and test all code
.PHONY: audit
audit: fmt tidy lint vet test
	@echo 'Auditing code done'

## vet: vetting code
.PHONY: vet
vet:
	@echo 'Vetting code...'
	@go vet $(shell go list ./... | grep -v /vendor/|xargs echo)

## test: test all code
.PHONY: test
test:
	@echo 'Running tests...'
	@CGO_ENABLED=0 go test $(shell go list ./... | grep -v /vendor/|xargs echo) -cover -coverprofile=cover.out
	go tool cover -func=cover.out

## fmt: formatting code
fmt:
	@echo 'Formatting code...'
	@go fmt $(shell go list ./... | grep -v /vendor/|xargs echo)

## vendor: tidy dependencies
.PHONY: tidy
tidy:
	@echo 'Tidying and verifying module dependencies...'
	@go mod tidy
	@go mod verify

## lint: linting code
.PHONY: lint
lint: bin/golangci-lint ## Run linter
	@echo 'Linting code...'
	golangci-lint run

## db/migrations/new name=$1: create a new database migration
.PHONY: db/migrations/new
db/migrations/new: bin/migrate
	@echo 'Creating migration files for ${name}...'
	migrate create -seq -ext=.sql -dir=./db/migrations ${name}

## db/migrations/up: apply all up database migrations
.PHONY: db/migrations/up
db/migrations/up: bin/migrate confirm
	@echo 'Running up migrations...'
	migrate -path ./db/migrations -database ${SIMPLERESTAPI_DSN} up

## seeds: seeds mean seeding application data, as of know only add user admin
.PHONY: seeds
seeds:
	@go run db/seed/main.go

## run: run application locally
.PHONY: run
run:
	@go run main.go

## build: build an application binary
.PHONY: build
build:
	@CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o simple-rest-api main.go
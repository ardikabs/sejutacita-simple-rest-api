package handler

import (
	"context"
	"errors"
	"simple-rest-api/domain"
)

func ExtractUserFromContext(ctx context.Context) (*domain.User, error) {
	val := ctx.Value(domain.ContextKeyUser)

	if user, ok := val.(*domain.User); ok {
		return user, nil
	}

	return nil, errors.New("unauthorized access")
}

func NewError(err error) map[string]interface{} {
	errs := make(map[string]interface{})
	errs["error"] = err.Error()
	return errs
}

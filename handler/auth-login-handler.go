package handler

import (
	"net/http"
	"simple-rest-api/service"

	"github.com/labstack/echo/v4"
)

type AuthLoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func AuthLogin(userSvc *service.UserService) echo.HandlerFunc {
	return func(c echo.Context) error {
		var input AuthLoginRequest
		if err := c.Bind(&input); err != nil {
			return err
		}

		ctx := c.Request().Context()
		jwtToken, err := userSvc.Login(ctx, input.Email, input.Password)
		if err != nil {
			return c.JSON(http.StatusBadRequest, NewError(err))
		}

		resp := make(map[string]interface{})
		resp["access_token"] = jwtToken.AccessToken
		resp["refresh_token"] = jwtToken.RefreshToken
		return c.JSON(http.StatusOK, resp)
	}
}

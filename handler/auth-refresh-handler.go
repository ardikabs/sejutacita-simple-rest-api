package handler

import (
	"net/http"
	"simple-rest-api/service"

	"github.com/labstack/echo/v4"
)

type AuthRefreshRequest struct {
	Token string `json:"token"`
}

func AuthRefresh(userSvc *service.UserService) echo.HandlerFunc {
	return func(c echo.Context) error {
		var input AuthRefreshRequest

		if err := c.Bind(&input); err != nil {
			return err
		}

		ctx := c.Request().Context()
		jwtToken, err := userSvc.Refresh(ctx, input.Token)
		if err != nil {
			return c.JSON(http.StatusBadRequest, NewError(err))
		}

		resp := make(map[string]interface{})
		resp["access_token"] = jwtToken.AccessToken
		resp["refresh_token"] = jwtToken.RefreshToken
		return c.JSON(http.StatusOK, resp)
	}
}

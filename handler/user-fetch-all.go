package handler

import (
	"net/http"
	"simple-rest-api/domain"
	"simple-rest-api/service"

	"github.com/labstack/echo/v4"
)

type UserFetchResponse struct {
	ID    uint64 `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
}

func UserFetchAll(userSvc *service.UserService) echo.HandlerFunc {
	return func(c echo.Context) error {

		ctx := c.Request().Context()
		user, err := ExtractUserFromContext(ctx)
		if err != nil {
			return c.JSON(http.StatusUnauthorized, err)
		}

		if user.IsAnonymous() {
			return c.JSON(http.StatusUnauthorized, err)
		}

		users, err := userSvc.GetAll(ctx)
		if err != nil {
			return err
		}

		resp := make(map[string]interface{})
		resp["data"] = createUserFetchAllResponses(users)
		return c.JSON(http.StatusOK, resp)
	}
}

func createUserFetchAllResponses(users []*domain.User) []UserFetchResponse {
	out := make([]UserFetchResponse, 0)
	for _, user := range users {
		out = append(out, UserFetchResponse{
			ID:    user.ID,
			Name:  user.Name,
			Email: user.Email,
		})
	}

	return out
}

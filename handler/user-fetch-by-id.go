package handler

import (
	"database/sql"
	"errors"
	"net/http"
	"simple-rest-api/service"
	"strconv"

	"github.com/labstack/echo/v4"
)

func UserFetchByID(userSvc *service.UserService) echo.HandlerFunc {
	return func(c echo.Context) error {

		ctx := c.Request().Context()
		str := c.Param("id")
		id, err := strconv.Atoi(str)
		if err != nil {
			return c.JSON(http.StatusBadRequest, NewError(err))
		}

		user, err := ExtractUserFromContext(ctx)
		if err != nil {
			return c.JSON(http.StatusUnauthorized, NewError(err))
		}

		if user.IsAnonymous() {
			return c.JSON(http.StatusUnauthorized, NewError(err))
		}

		out, err := userSvc.GetByID(ctx, uint64(id))
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				return c.JSON(http.StatusUnauthorized, NewError(errors.New("forbidden access")))
			}
			return c.JSON(http.StatusUnauthorized, NewError(err))
		}

		if !user.Permissions.Include("user:admin") && (user.ID != out.ID) {
			return c.JSON(http.StatusForbidden, NewError(errors.New("forbidden access")))
		}

		resp := make(map[string]interface{})
		resp["data"] = UserFetchResponse{
			ID:    out.ID,
			Name:  out.Name,
			Email: out.Email,
		}
		return c.JSON(http.StatusOK, resp)
	}
}

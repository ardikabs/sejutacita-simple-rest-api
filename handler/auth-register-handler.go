package handler

import (
	"errors"
	"net/http"
	"simple-rest-api/domain"
	"simple-rest-api/service"

	"github.com/labstack/echo/v4"
)

type AuthRegisterRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func AuthRegister(userSvc *service.UserService) echo.HandlerFunc {
	return func(c echo.Context) error {
		var in AuthRegisterRequest

		if err := c.Bind(&in); err != nil {
			return err
		}

		ctx := c.Request().Context()

		user := &domain.User{
			Email: in.Email,
		}

		if err := user.Password.Set(in.Password); err != nil {
			return c.JSON(http.StatusBadRequest, errors.New("bad password"))
		}

		if err := userSvc.Register(ctx, user); err != nil {
			return c.JSON(http.StatusBadRequest, NewError(err))
		}

		return c.NoContent(http.StatusAccepted)
	}
}

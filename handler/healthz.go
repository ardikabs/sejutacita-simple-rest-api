package handler

import (
	"database/sql"
	"net/http"

	"github.com/labstack/echo/v4"
)

func Healthz(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {

		ctx := c.Request().Context()

		if err := db.PingContext(ctx); err != nil {
			return c.String(http.StatusServiceUnavailable, "UNHEALTHY")
		}

		return c.String(http.StatusOK, "OK")
	}
}

package decoder

import (
	"fmt"
	"simple-rest-api/domain"

	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
)

type JWTDecoder interface {
	Decode(token, secret string) (*domain.User, error)
}

type DefaultJWTDecoder struct{}

func NewJWTDecoder() JWTDecoder {
	return &DefaultJWTDecoder{}
}

func (d *DefaultJWTDecoder) Decode(tokenString, secret string) (*domain.User, error) {
	token, err := VerifyJWT(tokenString, secret)
	if err != nil {
		return nil, err
	}

	return &domain.User{
		ID:          token.UserID,
		Email:       token.Email,
		Permissions: token.Permissions,
	}, nil
}

func VerifyJWT(tokenString, secret string) (*domain.JSONWebToken, error) {
	jwtKeyFunc := func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			terr := errors.New(fmt.Sprintf("unexpected signing method for jwt: %v", token.Header["alg"]))
			return nil, terr
		}

		return []byte(secret), nil
	}

	token, err := jwt.Parse(tokenString, jwtKeyFunc)
	if err != nil {
		if jwterr, ok := err.(*jwt.ValidationError); ok {
			switch jwterr.Errors {
			case jwt.ValidationErrorSignatureInvalid:
				return nil, errors.New("jwt invalid")
			case jwt.ValidationErrorMalformed:
				return nil, errors.New("jwt invalid")
			case jwt.ValidationErrorExpired:
				return nil, errors.New("unauthorized access")
			default:
				return nil, errors.New("unauthorized access")
			}
		}

		return nil, errors.New("internal server error - please contact support")
	}

	if !token.Valid {
		return nil, errors.New("jwt invalid")
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return nil, errors.New("jwt invalid")
	}

	td := &domain.JSONWebToken{}

	if val, ok := claims["user_id"]; ok {
		if id, ok := val.(float64); ok {
			td.UserID = uint64(id)
		}
	}

	if val, ok := claims["access_uuid"]; ok {
		if str, ok := val.(string); ok {
			td.AccessUuid = str
		}
	}

	if val, ok := claims["refresh_uuid"]; ok {
		if str, ok := val.(string); ok {
			td.RefreshUuid = str
		}
	}

	if val, ok := claims["email"]; ok {
		if str, ok := val.(string); ok {
			td.Email = str
		}
	}

	if val, ok := claims["permissions"]; ok {
		if permissions, ok := val.([]interface{}); ok {
			for _, permission := range permissions {
				td.Permissions = append(td.Permissions, permission.(string))
			}
		}
	}
	return td, nil
}
